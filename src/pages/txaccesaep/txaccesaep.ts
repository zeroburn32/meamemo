import { Component } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TxaccesaepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-txaccesaep',
  templateUrl: 'txaccesaep.html',
})
export class TxaccesaepPage {
  displayData = [];
  provinceData = [];
  communeData = [];
  pet: string = "region";
  term1: string = "";
  term2: string = "";
  term3: string = "";
  overlayHidden: boolean = false;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TxaccesaepPage');
    
    this.http.get('assets/data/txacces-region.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.displayData = parsed_data;
    });

    this.http.get('assets/data/tauxaccesprovince.json')
    .map( data => data.json() )
    .subscribe( parsed_data1 => {
      this.provinceData = parsed_data1;
      console.log(this.provinceData);
    });

    this.http.get('assets/data/tauxaccescommune.json')
    .map( data => data.json() )
    .subscribe( parsed_data2 => {
      console.log("Got data");
        console.log(parsed_data2);
      this.communeData = parsed_data2;
      console.log(this.communeData);
    });

  }

  openChartAep() {
    this.navCtrl.push('ChartaepPage');
    }
    public hideOverlay() {
      this.overlayHidden = true;
  }

}
