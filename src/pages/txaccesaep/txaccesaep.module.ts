import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TxaccesaepPage } from './txaccesaep';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
  declarations: [
    TxaccesaepPage,
  ],
  imports: [
    IonicPageModule.forChild(TxaccesaepPage),
    Ng2SearchPipeModule
  ],
})
export class TxaccesaepPageModule {}
