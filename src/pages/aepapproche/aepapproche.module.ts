import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AepapprochePage } from './aepapproche';

@NgModule({
  declarations: [
    AepapprochePage,
  ],
  imports: [
    IonicPageModule.forChild(AepapprochePage),
  ],
})
export class AepapprochePageModule {}
