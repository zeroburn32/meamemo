import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AeuefinancePage } from './aeuefinance';

@NgModule({
  declarations: [
    AeuefinancePage,
  ],
  imports: [
    IonicPageModule.forChild(AeuefinancePage),
  ],
})
export class AeuefinancePageModule {}
