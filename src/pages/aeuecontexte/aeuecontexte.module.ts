import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AeuecontextePage } from './aeuecontexte';

@NgModule({
  declarations: [
    AeuecontextePage,
  ],
  imports: [
    IonicPageModule.forChild(AeuecontextePage),
  ],
})
export class AeuecontextePageModule {}
