import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TxaccesaeuePage } from './txaccesaeue';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    TxaccesaeuePage,
  ],
  imports: [
    IonicPageModule.forChild(TxaccesaeuePage),
    Ng2SearchPipeModule
  ],
})
export class TxaccesaeuePageModule {}
