import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the TxaccesaeuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-txaccesaeue',
  templateUrl: 'txaccesaeue.html',
})
export class TxaccesaeuePage {
  regionData = [];
  provinceData = [];
  communeData = [];
  pet: string = "region";
  term1: string = "";
  term2: string = "";
  term3: string = "";
  overlayHidden: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TxaccesaeuePage');
    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.regionData = parsed_data;
    });

    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data1 => {
      this.provinceData = parsed_data1;
      console.log(this.provinceData);
    });

    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data2 => {
      console.log("Got data");
        console.log(parsed_data2);
      this.communeData = parsed_data2;
      console.log(this.communeData);
    });
  }

  openChartAep() {
    this.navCtrl.push('ChartaepPage');
    }
    public hideOverlay() {
      this.overlayHidden = true;
  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'No data!',
      subTitle: 'Aucune donnée à afficher',
      buttons: ['OK']
    });
    alert.present();
  }
}
