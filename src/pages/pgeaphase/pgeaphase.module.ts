import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeaphasePage } from './pgeaphase';

@NgModule({
  declarations: [
    PgeaphasePage,
  ],
  imports: [
    IonicPageModule.forChild(PgeaphasePage),
  ],
})
export class PgeaphasePageModule {}
