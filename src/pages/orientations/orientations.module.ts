import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrientationsPage } from './orientations';

@NgModule({
  declarations: [
    OrientationsPage,
  ],
  imports: [
    IonicPageModule.forChild(OrientationsPage),
  ],
})
export class OrientationsPageModule {}
