import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the OrientationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orientations',
  templateUrl: 'orientations.html',
})
export class OrientationsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrientationsPage');
  }
  
   goBack() {
    this.navCtrl.pop();
  } 
  openStrategies () {
  this.navCtrl.push('StrategiesPage');
  }

}
