import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the StrategiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-strategies',
  templateUrl: 'strategies.html',
})
export class StrategiesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StrategiesPage');
  }
   goBack() {
    this.navCtrl.pop();
  } 
  openProgrammes () {
  this.navCtrl.push('ProgrammesPage');
  }

}
