import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AeuestratPage } from './aeuestrat';

@NgModule({
  declarations: [
    AeuestratPage,
  ],
  imports: [
    IonicPageModule.forChild(AeuestratPage),
  ],
})
export class AeuestratPageModule {}
