import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TxFonctPemPage } from './tx-fonct-pem';
import { Ng2SearchPipeModule } from 'ng2-search-filter';



@NgModule({
  declarations: [
    TxFonctPemPage,
  ],
  imports: [
    IonicPageModule.forChild(TxFonctPemPage),
    Ng2SearchPipeModule
  ],
})
export class TxFonctPemPageModule {}
