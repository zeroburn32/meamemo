import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GirefinancePage } from './girefinance';

@NgModule({
  declarations: [
    GirefinancePage,
  ],
  imports: [
    IonicPageModule.forChild(GirefinancePage),
  ],
})
export class GirefinancePageModule {}
