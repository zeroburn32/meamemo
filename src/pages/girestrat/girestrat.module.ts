import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GirestratPage } from './girestrat';

@NgModule({
  declarations: [
    GirestratPage,
  ],
  imports: [
    IonicPageModule.forChild(GirestratPage),
  ],
})
export class GirestratPageModule {}
