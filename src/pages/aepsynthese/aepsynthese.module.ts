import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AepsynthesePage } from './aepsynthese';

@NgModule({
  declarations: [
    AepsynthesePage,
  ],
  imports: [
    IonicPageModule.forChild(AepsynthesePage),
  ],
})
export class AepsynthesePageModule {}
