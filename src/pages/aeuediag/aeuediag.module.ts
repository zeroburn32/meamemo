import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AeuediagPage } from './aeuediag';

@NgModule({
  declarations: [
    AeuediagPage,
  ],
  imports: [
    IonicPageModule.forChild(AeuediagPage),
  ],
})
export class AeuediagPageModule {}
