import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnaeuemenuPage } from './pnaeuemenu';

@NgModule({
  declarations: [
    PnaeuemenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PnaeuemenuPage),
  ],
})
export class PnaeuemenuPageModule {}
