import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PnaeuemenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pnaeuemenu',
  templateUrl: 'pnaeuemenu.html',
})
export class PnaeuemenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PnaeuemenuPage');
  }
  goPnaeueDetails () {
    this.navCtrl.push('PnaeueDetailsPage');
    }
  
    goAeueActions() {
      this.navCtrl.push('AeueactionsPage');
    }
    goAeueContexte() {
      this.navCtrl.push('AeuecontextePage');
    }
    goAeueDiag() {
      this.navCtrl.push('AeuediagPage');
    }
    goAeueStrat() {
      this.navCtrl.push('AeuestratPage');
    }
    goAeueFinance() {
      this.navCtrl.push('AeuefinancePage');
    }
    
}
