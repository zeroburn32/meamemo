import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AepactionsPage } from './aepactions';

@NgModule({
  declarations: [
    AepactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AepactionsPage),
  ],
})
export class AepactionsPageModule {}
