import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeainstitutionPage } from './pgeainstitution';

@NgModule({
  declarations: [
    PgeainstitutionPage,
  ],
  imports: [
    IonicPageModule.forChild(PgeainstitutionPage),
  ],
})
export class PgeainstitutionPageModule {}
