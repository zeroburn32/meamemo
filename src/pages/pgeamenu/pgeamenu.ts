import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PgeamenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pgeamenu',
  templateUrl: 'pgeamenu.html',
})
export class PgeamenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PgeamenuPage');
  }
  goPgeaResults() {
    this.navCtrl.push('PgearesultsPage');
  }
  goPgeaActions() {
    this.navCtrl.push('PgeaactionsPage');
  }
  goPgeaPhase() {
    this.navCtrl.push('PgeaphasePage');
  }
  goPgeaInsitution() {
    this.navCtrl.push('PgeainstitutionPage');
  }
  goPgeaStrat() {
    this.navCtrl.push('PgeastratPage');
  }
  goPgeaFinance() {
    this.navCtrl.push('PgeafinancePage');
  }
  goPgeaDetails() {
    this.navCtrl.push('PgeaDetailsPage');
  }
  goPgeaTab2() {
    this.navCtrl.push('Pgeatab2Page');
  }
  goPgeaTab3() {
    this.navCtrl.push('Pgeatab3Page');
  }
}
