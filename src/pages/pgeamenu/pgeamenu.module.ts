import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeamenuPage } from './pgeamenu';

@NgModule({
  declarations: [
    PgeamenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PgeamenuPage),
  ],
})
export class PgeamenuPageModule {}
