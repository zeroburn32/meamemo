import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnahmenuPage } from './pnahmenu';

@NgModule({
  declarations: [
    PnahmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PnahmenuPage),
  ],
})
export class PnahmenuPageModule {}
