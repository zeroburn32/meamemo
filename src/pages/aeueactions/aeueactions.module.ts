import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AeueactionsPage } from './aeueactions';

@NgModule({
  declarations: [
    AeueactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AeueactionsPage),
  ],
})
export class AeueactionsPageModule {}
