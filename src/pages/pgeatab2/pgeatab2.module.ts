import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Pgeatab2Page } from './pgeatab2';

@NgModule({
  declarations: [
    Pgeatab2Page,
  ],
  imports: [
    IonicPageModule.forChild(Pgeatab2Page),
  ],
})
export class Pgeatab2PageModule {}
