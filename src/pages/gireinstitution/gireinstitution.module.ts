import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GireinstitutionPage } from './gireinstitution';

@NgModule({
  declarations: [
    GireinstitutionPage,
  ],
  imports: [
    IonicPageModule.forChild(GireinstitutionPage),
  ],
})
export class GireinstitutionPageModule {}
