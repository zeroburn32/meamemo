import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeastratPage } from './pgeastrat';

@NgModule({
  declarations: [
    PgeastratPage,
  ],
  imports: [
    IonicPageModule.forChild(PgeastratPage),
  ],
})
export class PgeastratPageModule {}
