import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Pgeatab3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pgeatab3',
  templateUrl: 'pgeatab3.html',
})
export class Pgeatab3Page {
  pgeaabudget =[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Pgeatab3Page');
    this.http.get('assets/data/pgeabudget/pgeabudget.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.pgeaabudget = parsed_data;
      console.log(this.pgeaabudget);
    });
  }

}
