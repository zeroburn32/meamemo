import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Pgeatab3Page } from './pgeatab3';

@NgModule({
  declarations: [
    Pgeatab3Page,
  ],
  imports: [
    IonicPageModule.forChild(Pgeatab3Page),
  ],
})
export class Pgeatab3PageModule {}
