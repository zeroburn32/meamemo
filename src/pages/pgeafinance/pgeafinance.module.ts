import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeafinancePage } from './pgeafinance';

@NgModule({
  declarations: [
    PgeafinancePage,
  ],
  imports: [
    IonicPageModule.forChild(PgeafinancePage),
  ],
})
export class PgeafinancePageModule {}
