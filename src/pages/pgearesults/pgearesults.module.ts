import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgearesultsPage } from './pgearesults';

@NgModule({
  declarations: [
    PgearesultsPage,
  ],
  imports: [
    IonicPageModule.forChild(PgearesultsPage),
  ],
})
export class PgearesultsPageModule {}
