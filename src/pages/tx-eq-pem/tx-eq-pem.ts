import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the TxEqPemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tx-eq-pem',
  templateUrl: 'tx-eq-pem.html',
})
export class TxEqPemPage {
  regionData = [];
  provinceData = [];
  communeData = [];
  pet: string = "region";
  term1: string = "";
  term2: string = "";
  term3: string = "";
  overlayHidden: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TxEqPemPage');
    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.regionData = parsed_data;
    });

    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data1 => {
      this.provinceData = parsed_data1;
      console.log(this.provinceData);
    });

    this.http.get('assets/data/TxFonctAepsReg.json')
    .map( data => data.json() )
    .subscribe( parsed_data2 => {
      console.log("Got data");
        console.log(parsed_data2);
      this.communeData = parsed_data2;
      console.log(this.communeData);
    });
  }

  openChartAeps() {
    this.navCtrl.push('ChartAepsPage');
    }
    public hideOverlay() {
      this.overlayHidden = true;
  }

}
