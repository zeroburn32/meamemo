import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TxEqPemPage } from './tx-eq-pem';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    TxEqPemPage,
  ],
  imports: [
    IonicPageModule.forChild(TxEqPemPage),
    Ng2SearchPipeModule
  ],
})
export class TxEqPemPageModule {}
