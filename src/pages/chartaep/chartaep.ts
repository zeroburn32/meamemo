import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Chart } from 'chart.js';



/**
 * Generated class for the ChartaepPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chartaep',
  templateUrl: 'chartaep.html',
})
export class ChartaepPage {
  @ViewChild('barCanvas') barCanvas;
  @ViewChild('barCanvas') barCanvas1;
  barChart: any;
  barChart1: any;
  regionData = [];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    
  }

  ionViewDidLoad() {    
    console.log('ionViewDidLoad ChartaepPage');
    
    this.http.get('assets/data/tauxaccesregioncolumn.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.regionData = parsed_data;
    });
    this.barChart = new Chart(this.barCanvas.nativeElement, {
 
      type: 'horizontalBar',
      data: {
          labels: ["B. DU MOUHOUN","CASCADES","CENTRE","CENTRE-EST","CENTRE-NORD","CENTRE-OUEST","CENTRE-SUD","EST","HAUTS-BASSINS","NORD","PLATEAU CENTRAL","SAHEL","SUD-OUEST","BURKINA FASO"],
          datasets: [{
              label: 'Taux Accès',
              data: [64.0,57.1,90.0,74.5,70.6,68.5,84.5,52.8,50.8,75.0,81.8,56.7,71.8,66.2],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }

  });

}

  /*this.lockLandscape();

  this.screenOrientation.onChange().subscribe(
    value => alert('Orientation changed'),
    error => alert('Changed error: ' + error),
    () => alert('Done')
  )


  }

  lockLandscape() {
    alert('Orientation locked landscape.');
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }
  unlock() {
    alert('Orientation unlocked');
    this.screenOrientation.unlock();
  }*/
}
