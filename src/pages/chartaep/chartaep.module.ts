import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartaepPage } from './chartaep';

@NgModule({
  declarations: [
    ChartaepPage,
  ],
  imports: [
    IonicPageModule.forChild(ChartaepPage),
  ],
})
export class ChartaepPageModule {}
