import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartPemPage } from './chart-pem';

@NgModule({
  declarations: [
    ChartPemPage,
  ],
  imports: [
    IonicPageModule.forChild(ChartPemPage),
  ],
})
export class ChartPemPageModule {}
