import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Chart } from 'chart.js';
/**
 * Generated class for the ChartPemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chart-pem',
  templateUrl: 'chart-pem.html',
})
export class ChartPemPage {
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  regionData = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartPemPage');
    this.http.get('assets/data/tauxaccesregioncolumn.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.regionData = parsed_data;
    });
    this.barChart = new Chart(this.barCanvas.nativeElement, {
 
      type: 'horizontalBar',
      data: {
          labels: ["BMH","CASCADES","CENTRE","CENTRE-EST","CENTRE-NORD","CENTRE-OUEST","CENTRE-SUD","EST","HAUTS-BASSINS","NORD","PLATEAU CENTRAL","SAHEL","SUD-OUEST","BURKINA FASO"],
          datasets: [{
              label: 'Taux ',
              data: [81.5,94.6,86.7,97.5,92.8,83.8,91.9,93.8,80.9,81.5,90.9,87.0,87.4,88.8],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }

  });
  }

}
