import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { Chart } from 'chart.js';

/**
 * Generated class for the ChartAepsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chart-aeps',
  templateUrl: 'chart-aeps.html',
})
export class ChartAepsPage {
  @ViewChild('barCanvas') barCanvas;
  barChart: any;
  regionData = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChartAepsPage');
    this.http.get('assets/data/tauxaccesregioncolumn.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.regionData = parsed_data;
    });
    this.barChart = new Chart(this.barCanvas.nativeElement, {
 
      type: 'horizontalBar',
      data: {
          labels: ["BMH","CASCADES","CENTRE","CENTRE-EST","CENTRE-NORD","CENTRE-OUEST","CENTRE-SUD","EST","HAUTS-BASSINS","NORD","PLATEAU CENTRAL","SAHEL","SUD-OUEST","BURKINA FASO"],
          datasets: [{
              label: 'Taux ',
              data: [77.9,85.7,95.2,84.9,85.1,88.5,92.4,92.4,87.9,89.1,83.7,86.1,100,87.7],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
      }

  });
  }
}
