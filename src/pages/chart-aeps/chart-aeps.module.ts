import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartAepsPage } from './chart-aeps';

@NgModule({
  declarations: [
    ChartAepsPage,
  ],
  imports: [
    IonicPageModule.forChild(ChartAepsPage),
  ],
})
export class ChartAepsPageModule {}
