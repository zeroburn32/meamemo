import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeaDetailsPage } from './pgea-details';

@NgModule({
  declarations: [
    PgeaDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PgeaDetailsPage),
  ],
})
export class PgeaDetailsPageModule {}
