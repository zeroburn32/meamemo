import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnaepmenuPage } from './pnaepmenu';

@NgModule({
  declarations: [
    PnaepmenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PnaepmenuPage),
  ],
})
export class PnaepmenuPageModule {}
