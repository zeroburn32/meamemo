import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PnaepmenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pnaepmenu',
  templateUrl: 'pnaepmenu.html',
})
export class PnaepmenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PnaepmenuPage');
  }
  goPnaepDetails () {
    this.navCtrl.push('PnaepDetailsPage');
    }
  goAepConexte() {
    this.navCtrl.push('AepcontextePage');
  }
  goAepActions() {
    this.navCtrl.push('AepactionsPage');
  }
  goAepApproche() {
    this.navCtrl.push('AepapprochePage');
  }
  goAepFinance() {
    this.navCtrl.push('AepfinancePage');
  }
  goAepSynthese() {
    this.navCtrl.push('AepsynthesePage');
  }
}
