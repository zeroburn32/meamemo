import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnahDetailsPage } from './pnah-details';

@NgModule({
  declarations: [
    PnahDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PnahDetailsPage),
  ],
})
export class PnahDetailsPageModule {}
