import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GirecontextePage } from './girecontexte';

@NgModule({
  declarations: [
    GirecontextePage,
  ],
  imports: [
    IonicPageModule.forChild(GirecontextePage),
  ],
})
export class GirecontextePageModule {}
