import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AepfinancePage } from './aepfinance';

@NgModule({
  declarations: [
    AepfinancePage,
  ],
  imports: [
    IonicPageModule.forChild(AepfinancePage),
  ],
})
export class AepfinancePageModule {}
