import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnaepDetailsPage } from './pnaep-details';

@NgModule({
  declarations: [
    PnaepDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PnaepDetailsPage),
  ],
})
export class PnaepDetailsPageModule {}
