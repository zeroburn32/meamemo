import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PnaepDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pnaep-details',
  templateUrl: 'pnaep-details.html',
})
export class PnaepDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PnaepDetailsPage');
  }
  openIndicateurs () {
    this.navCtrl.push('IndicateursPage');
    }

}
