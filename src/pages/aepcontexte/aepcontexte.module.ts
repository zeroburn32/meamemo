import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AepcontextePage } from './aepcontexte';

@NgModule({
  declarations: [
    AepcontextePage,
  ],
  imports: [
    IonicPageModule.forChild(AepcontextePage),
  ],
})
export class AepcontextePageModule {}
