import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndicateursPage } from './indicateurs';

@NgModule({
  declarations: [
    IndicateursPage,
  ],
  imports: [
    IonicPageModule.forChild(IndicateursPage),
  ],
})
export class IndicateursPageModule {}
