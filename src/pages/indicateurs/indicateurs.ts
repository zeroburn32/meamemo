import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


/**
 * Generated class for the IndicateursPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-indicateurs',
  templateUrl: 'indicateurs.html',
})
export class IndicateursPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndicateursPage');
  }
  openTxAccesAep () {
    this.navCtrl.push('TxaccesaepPage');
    }
  openTxAccesAeue () {
      this.navCtrl.push('TxaccesaeuePage');
      }
  openTxFoncPem () {
      this.navCtrl.push('TxFonctPemPage');
   }
   openTxEqPem () {
      this.navCtrl.push('TxEqPemPage');
   }
   openHome () {
    this.navCtrl.push(HomePage);
 }

}
