import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PngireDetailsPage } from './pngire-details';

@NgModule({
  declarations: [
    PngireDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PngireDetailsPage),
  ],
})
export class PngireDetailsPageModule {}
