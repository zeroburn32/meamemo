import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PngiremenuPage } from './pngiremenu';

@NgModule({
  declarations: [
    PngiremenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PngiremenuPage),
  ],
})
export class PngiremenuPageModule {}
