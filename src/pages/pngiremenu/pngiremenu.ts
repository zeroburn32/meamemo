import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PngiremenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pngiremenu',
  templateUrl: 'pngiremenu.html',
})
export class PngiremenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PngiremenuPage');
  }
  goGireDetails () {
    this.navCtrl.push('PngireDetailsPage');
    } 
  goGireStrat () {
      this.navCtrl.push('GirestratPage');
    } 
  goGireInsitution () {
      this.navCtrl.push('GireinstitutionPage');
    } 
  goGireFinance () {
      this.navCtrl.push('GirefinancePage');
    } 
  goGireContexte () {
      this.navCtrl.push('GirecontextePage');
    } 

}
