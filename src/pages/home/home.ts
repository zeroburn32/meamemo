import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'

})

export class HomePage {

  constructor(public navCtrl: NavController) {
    
  }
  openStrategies () {
  this.navCtrl.push('StrategiesPage');
  } 
  openOrientations () {
  this.navCtrl.push('OrientationsPage');
  }
  openVision () {
  this.navCtrl.push('VisionPage');
  }
  openDefis () {
  this.navCtrl.push('DefisPage');
  }
  openProgrammes () {
  this.navCtrl.push('ProgrammesPage');
  }
  openIndicateurs () {
    this.navCtrl.push('IndicateursPage');
    }
         
}
 
 