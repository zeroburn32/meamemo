import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PgeaactionsPage } from './pgeaactions';

@NgModule({
  declarations: [
    PgeaactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PgeaactionsPage),
  ],
})
export class PgeaactionsPageModule {}
