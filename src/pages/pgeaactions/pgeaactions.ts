import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the PgeaactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pgeaactions',
  templateUrl: 'pgeaactions.html',
})
export class PgeaactionsPage {
  pgeaactions =[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PgeaactionsPage');
    this.http.get('assets/data/pgeaactions/pgeaactions.json')
    .map( data => data.json() )
    .subscribe( parsed_data => {
      this.pgeaactions = parsed_data;
      console.log(this.pgeaactions);
    });
  }

}
