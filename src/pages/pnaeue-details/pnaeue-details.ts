import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PnaeueDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pnaeue-details',
  templateUrl: 'pnaeue-details.html',
})
export class PnaeueDetailsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PnaeueDetailsPage');
  }
  openIndicateurs () {
    this.navCtrl.push('IndicateursPage');
    }
  

}
