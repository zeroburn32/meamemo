import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PnaeueDetailsPage } from './pnaeue-details';

@NgModule({
  declarations: [
    PnaeueDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PnaeueDetailsPage),
  ],
})
export class PnaeueDetailsPageModule {}
