import { IndicateursPage } from './../pages/indicateurs/indicateurs';
import { DefisPage } from './../pages/defis/defis';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { OrientationsPage } from '../pages/orientations/orientations';
import { StrategiesPage } from '../pages/strategies/strategies';
import { ProgrammesPage } from '../pages/programmes/programmes';
import { VisionPage } from '../pages/vision/vision';
import { AproposPage } from '../pages/apropos/apropos';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
      
    this.pages = [
      { title: 'Accueil', component: HomePage },
      { title: 'Les grands défis', component: DefisPage },
      { title: 'Vision SNE', component: VisionPage },
      { title: 'Orientations', component: OrientationsPage },
      { title: 'Choix stratégiques', component: StrategiesPage },
      { title: 'Programmes Budg.', component: ProgrammesPage },
      { title: 'Indicateurs Clés', component: IndicateursPage },
      { title: 'A propos', component: AproposPage }
    ];
    
          

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.splashScreen.show();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
