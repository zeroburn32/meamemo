import { Pgeatab3PageModule } from './../pages/pgeatab3/pgeatab3.module';
import { Pgeatab2PageModule } from './../pages/pgeatab2/pgeatab2.module';
import { PgeaphasePageModule } from './../pages/pgeaphase/pgeaphase.module';
import { PgeainstitutionPageModule } from './../pages/pgeainstitution/pgeainstitution.module';
import { PgeastratPageModule } from './../pages/pgeastrat/pgeastrat.module';
import { PgeafinancePageModule } from './../pages/pgeafinance/pgeafinance.module';
import { PgeaactionsPageModule } from './../pages/pgeaactions/pgeaactions.module';
import { PgearesultsPageModule } from './../pages/pgearesults/pgearesults.module';
import { GirefinancePageModule } from './../pages/girefinance/girefinance.module';
import { GirecontextePageModule } from './../pages/girecontexte/girecontexte.module';
import { AeueactionsPageModule } from './../pages/aeueactions/aeueactions.module';
import { AeuediagPageModule } from './../pages/aeuediag/aeuediag.module';
import { AeuestratPageModule } from './../pages/aeuestrat/aeuestrat.module';
import { AeuefinancePageModule } from './../pages/aeuefinance/aeuefinance.module';
import { AeuecontextePageModule } from './../pages/aeuecontexte/aeuecontexte.module';
import { AepfinancePageModule } from './../pages/aepfinance/aepfinance.module';
import { AepsynthesePageModule } from './../pages/aepsynthese/aepsynthese.module';
import { AepcontextePageModule } from './../pages/aepcontexte/aepcontexte.module';
import { PngiremenuPageModule } from './../pages/pngiremenu/pngiremenu.module';
import { PgeamenuPageModule } from './../pages/pgeamenu/pgeamenu.module';
import { PnahmenuPageModule } from './../pages/pnahmenu/pnahmenu.module';
import { PnaeuemenuPageModule } from './../pages/pnaeuemenu/pnaeuemenu.module';
import { PnaepmenuPageModule } from './../pages/pnaepmenu/pnaepmenu.module';
import { HomePageModule } from './../pages/home/home.module';
import { ChartAepsPageModule } from './../pages/chart-aeps/chart-aeps.module';
import { ChartPemPageModule } from './../pages/chart-pem/chart-pem.module';
import { TxEqPemPageModule } from './../pages/tx-eq-pem/tx-eq-pem.module';
import { TxFonctPemPageModule } from './../pages/tx-fonct-pem/tx-fonct-pem.module';
import { ChartaepPageModule } from './../pages/chartaep/chartaep.module';
import { TxaccesaepPageModule } from './../pages/txaccesaep/txaccesaep.module';
import { IndicateursPageModule } from './../pages/indicateurs/indicateurs.module';
import { DefisPageModule } from './../pages/defis/defis.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { OrientationsPageModule } from '../pages/orientations/orientations.module';
import { StrategiesPageModule } from '../pages/strategies/strategies.module';
import { VisionPageModule } from '../pages/vision/vision.module';
import { ProgrammesPageModule } from '../pages/programmes/programmes.module';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AproposPage } from '../pages/apropos/apropos';
import { HttpModule } from '@angular/http';
import { AepactionsPageModule } from '../pages/aepactions/aepactions.module';
import { AepapprochePageModule } from '../pages/aepapproche/aepapproche.module';
import { GirestratPageModule } from '../pages/girestrat/girestrat.module';
import { GireinstitutionPageModule } from '../pages/gireinstitution/gireinstitution.module';

@NgModule({
  declarations: [
    MyApp,
    AproposPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    OrientationsPageModule,
    StrategiesPageModule,
    ProgrammesPageModule,
    VisionPageModule,
    DefisPageModule,
    IndicateursPageModule,
    TxaccesaepPageModule,TxFonctPemPageModule,TxEqPemPageModule,
    ChartPemPageModule,ChartAepsPageModule, ChartaepPageModule,
    HomePageModule,
    PnaepmenuPageModule, PnaeuemenuPageModule, PnahmenuPageModule,PgeamenuPageModule,PngiremenuPageModule,
    AepcontextePageModule, AepsynthesePageModule, AepactionsPageModule,
    AepfinancePageModule,AepapprochePageModule,
    AeuecontextePageModule, AeuefinancePageModule,AeuestratPageModule, AeuediagPageModule, AeueactionsPageModule,
    GirecontextePageModule, GirestratPageModule, GireinstitutionPageModule, GirefinancePageModule,
    PgearesultsPageModule, PgeaactionsPageModule, PgeafinancePageModule, PgeastratPageModule, PgeainstitutionPageModule, PgeaphasePageModule,
    Pgeatab2PageModule, Pgeatab3PageModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AproposPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
